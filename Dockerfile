FROM docker.io/python:3.6-slim as python

FROM python as build

ARG app_name

ARG venv_dir=/opt/${app_name}/venv
ARG venv_python=${venv_dir}/bin/python

COPY --chown=root:root requirements.txt /var/tmp

RUN \
    mkdir -vp ${venv_dir} && \
    python3 -m venv ${venv_dir} && \
    ${venv_python} -m pip install --no-cache-dir -r /var/tmp/requirements.txt && \
    true

FROM python as runtime

ARG app_name

ARG venv_dir=/opt/${app_name}/venv
ARG venv_python=${venv_dir}/bin/python


COPY --from=build /opt/${app_name} /opt/${app_name}

ARG group_name=${app_name}
ARG user_name=${app_name}
ARG workdir=/var/opt/${app_name}

RUN \
    groupadd --system ${group_name} && \
    useradd --system --no-create-home --home-dir ${workdir} -G ${group_name} -g ${group_name} ${user_name} && \
    mkdir -vp ${workdir} && \
    chown -R ${user_name}:${group_name} ${workdir} && \
    true

COPY --chown=root:root *.whl /var/tmp
RUN \
    sha256sum /var/tmp/*.whl && \
    ${venv_python} -m pip install --no-deps --no-cache-dir \
        /var/tmp/*.whl

ENV PYTHONUNBUFFERED=1 \
    PYTHONDONTWRITEBYTECODE=1


USER ${user_name}:${group_name}
WORKDIR ${workdir}

HEALTHCHECK CMD ["/fixme", "healthcheck"]


ENTRYPOINT ["/fixme"]
CMD ["daemon", "run"]
