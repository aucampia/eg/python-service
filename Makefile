# https://www.gnu.org/software/make/manual/make.html
SHELL=bash
.SHELLFLAGS=-ec -o pipefail
current_makefile:=$(lastword $(MAKEFILE_LIST))
current_makefile_dir:=$(dir $(abspath $(current_makefile)))

.PHONY: all
all: ## do everything (default target)

########################################################################
# boilerplate
########################################################################

define __newline


endef

ifneq ($(filter all targets,$(VERBOSE)),)
__ORIGINAL_SHELL:=$(SHELL)
SHELL=$(warning Building $@$(if $<, (from $<))$(if $?, ($? newer)))$(TIME) $(__ORIGINAL_SHELL)
endif


skip=
# skipable makes the targets passed to it skipable with skip=foo%
# $(1): targets that should be skipable
skipable=$(filter-out $(skip),$(1))

########################################################################
# variables
########################################################################

app_name=iapyegservice
main_cmd=aucampia.eg.service.py
outputs_dir=var/outputs

stamps_dir=var/stamps

py_source=./src ./tests
venv_dir=.venv
poetry=python -m poetry

tools_dir=var/tools
generated_dir=generated
XDG_CACHE_HOME?=$(HOME)/.cache

giti_commit_hash=$(shell git log -1 --format="%H")

$(if $(wildcard default.env),$(eval include default.env),)
$(if $(wildcard .env),$(eval include .env),)
$(if $(and $(environment),$(wildcard env.$(environment))),$(eval include env.$(environment)),)

########################################################################
# targets ...
########################################################################

.PHONY: configure
configure: $(venv_dir) ## comfigure the project
poetry-install $(venv_dir): ## installs the project dependencies
	$(poetry) install

.PHONY: test
test: configure ## run the project's tests
	$(poetry) run pytest --cov-config=pyproject.toml --cov-report term --cov-report xml --cov=src ./tests

.PHONY: test-verbose
test-verbose: configure ## run the project's tests with verbose output
	$(poetry) run pytest -rA --log-level DEBUG --cov-config=pyproject.toml --cov-report term --cov-report xml --cov=src ./tests

.PHONY: validate-static
validate-static: configure ## perform static validation
	$(poetry) run mypy --show-error-codes --show-error-context \
		$(py_source)
	$(poetry) run codespell $(py_source)
	$(poetry) run isort --check $(py_source)
	$(poetry) run black --check $(py_source)
	$(poetry) run flake8 $(py_source)
	$(poetry) export --without-hashes --dev --format requirements.txt | $(poetry) run safety check --full-report --stdin

.PHONY: validate
validate: validate-static test ## validate
all: validate

.PHONY: validate-fix
validate-fix: configure ## fix auto-fixable validation errors
	$(poetry) run pycln --expand-stars --all $(py_source)
	$(poetry) run isort $(py_source)
	$(poetry) run black $(py_source)

.PHONY: clean
clean: clean-dist/ ## clean

.PHONY: distclean
distclean: clean clean-.venv/ ## clean everything

.PHONY: install-editable
install-editable: configure ## install as editable
	rm -rv src/*.egg-info/ || :
	## uninstall
	python -m pip uninstall -y "$$($(poetry) version | gawk '{ print $$1 }')"
	## install
	rm -rv dist/ || :
	$(poetry) build --format sdist \
		&& tar --wildcards -xvf dist/*.tar.gz -O '*/setup.py' > setup.py \
		&& python -m pip install --user --prefix="$${HOME}/.local/" --editable .

.PHONY: install-user
install-user: configure ## install as user package
	rm -rv src/*.egg-info/ || :
	## uninstall
	python -m pip uninstall -y "$$($(poetry) version | gawk '{ print $$1 }')"
	## install
	rm -rv dist/ || :
	$(poetry) build --format sdist \
		&& python -m pip install --user --prefix="$${HOME}/.local/" dist/*.tar.gz

run: ## run the service
	$(poetry) run $(main_cmd) -vvv run

################################################################################
# poetry
################################################################################

pip_compile=pipx run --python python3.6 --spec=pip-tools pip-compile

requirements-poetry.txt: requirements-poetry.in
	$(pip_compile) --quiet --generate-hashes --annotate --emit-options --output-file $(@) $(<)

.PHONY: install-poetry
toolchain: install-poetry
install-poetry: requirements-poetry.txt ## install poetry
	python -m pip install --require-hashes -r requirements-poetry.txt

.PHONY: update-latest
update-latest: ## update dependencies to latests
	dasel select -f pyproject.toml -m 'tool.poetry.dev-dependencies.-' \
		| sed 's/.*/&@latest/g' \
		| xargs -n1 echo $(poetry) add --dev
	dasel select -f pyproject.toml -m 'tool.poetry.dependencies.-' \
		| grep -v '^python' \
		| sed 's/.*/&@latest/g' \
		| xargs -n1 echo $(poetry) add

########################################################################
# gRPC
########################################################################

.PHONY: validate-proto
validate: validate-proto
validate-proto: $(prototool_bin) ## validate proto files
	$(prototool_bin) lint --error-format "filename:line:column:id:message"
	$(prototool_bin) format --lint

.PHONY: validate-proto-fix
validate-fix: validate-proto-fix
validate-proto-fix: $(prototool_bin) ## fix proto file validation errors
	$(prototool_bin) format --overwrite

proto_paths=spec/
proto_source_dirs=$(proto_paths)
proto_files_exclude=thirdparty/%
proto_files_include=%
# $1 = dirs $2 = patterns
rwildcard=$(wildcard $1$2) $(foreach d,$(wildcard $1*),$(call rwildcard,$d/,$2))
proto_files= \
	$(filter $(proto_files_include), \
		$(filter-out $(proto_files_exclude), \
			$(call rwildcard,$(proto_source_dirs),*.proto)))

.PHONY: generate-proto
generate-proto: | $(generated_dir)/ ## generate proto files
	$(poetry) run \
		protoc \
		--plugin=.venv/bin/protoc-gen-mypy \
		--plugin=.venv/bin/protoc-gen-mypy_grpc \
		--mypy_out=$(generated_dir)/ \
		--mypy_grpc_out=$(generated_dir)/ \
		$(foreach proto_path,$(proto_paths),--proto_path $(proto_path)) \
		$(proto_files)
	$(poetry) run python -m grpc.tools.protoc \
		--python_out=$(generated_dir)/ \
		--grpc_python_out=$(generated_dir)/ \
		$(foreach proto_path,$(proto_paths),--proto_path $(proto_path)) \
		$(proto_files)

########################################################################
# OCI
########################################################################


oci_context_dir=var/oci_context

$(oci_context_dir)/requirements.txt: poetry.lock | $(oci_context_dir)/
	$(poetry) export --with-credentials \
		| $(pip_compile) --quiet --generate-hashes --annotate --emit-options \
			--output-file $(@) /dev/stdin




$(oci_context_dir)/% : %
	cp $(<) $(@)

clean: clean-oci-context
clean-oci-context: clean-$(oci_context_dir)/

.PHONY: oci-context
oci-context: \
	$(oci_context_dir)/requirements.txt \
	$(oci_context_dir)/requirements-poetry.txt \
	oci-context-wheel \


#$(oci_context_dir)/package.whl: | $(oci_context_dir)/
oci-context-wheel: | $(oci_context_dir)/
	rm -vf $(oci_context_dir)/*.whl
	python -m build --wheel --outdir $(oci_context_dir)
	# mv $(oci_context_dir)/*.whl $(@)




docker=docker
docker_compose=docker-compose
dockerfile=Dockerfile

docker_image_build_cmd=DOCKER_BUILDKIT=1 $(docker) image build
docker_image_build_args=--progress plain
docker_image_build=$(docker_image_build_cmd) $(docker_image_build_args)

.PHONY: validate-dockerfile
validate: validate-dockerfile
validate-dockerfile: $(stamps_dir)/Dockerfile.valid
$(stamps_dir)/Dockerfile.valid: $(dockerfile) | $(stamps_dir)/
	$(docker_compose) run --rm -T hadolint < $(<)
	touch $(@)

oci_tag_prefix=

oci_tag_suffixes_const = \
	latest \

oci_tag_suffixes_git = \
	gitc-$(giti_commit_hash) \

oci_tag_suffixes = \
	$(oci_tag_suffixes_const) \
	$(oci_tag_suffixes_git) \

oci_refs_local=\
	$(foreach oci_tag_suffix,\
		$(oci_tag_suffixes),\
		ocreg.invalid/$(app_name):$(oci_tag_prefix)$(oci_tag_suffix))

oci_ref_names?=
oci_refs_remote=\
	$(foreach oci_tag_suffix,\
		$(oci_tag_suffixes),\
		$(foreach oci_ref_name,$(oci_ref_names),$(oci_ref_name):$(oci_tag_prefix)$(oci_tag_suffix)))

oci_refs = \
	$(oci_refs_local) \
	$(oci_refs_remote) \

$(outputs_dir)/stage-%.oci.tar: $(dockerfile) validate-dockerfile | $(outputs_dir)/ $(oci_context_dir)/
	$(docker_image_build) --force-rm \
		--cache-from ocreg.invalid/stages/$(app_name)/$(*):latest \
		--cache-from $(firstword $(oci_refs_local)) \
		--build-arg BUILDKIT_INLINE_CACHE=1 \
		--build-arg app_name=$(app_name) \
		--build-arg main_cmd=$(main_cmd) \
		--tag ocreg.invalid/stages/$(app_name)/$(*) \
		--target $(*) \
		--file $(<) \
		$(docker_image_build_extra_args) \
		$(oci_context_dir)
	$(docker) image save -o $(@) ocreg.invalid/stages/$(app_name)/$(*)

$(outputs_dir)/stage-build.oci.tar: $(oci_context_dir)/requirements.txt
$(outputs_dir)/stage-runtime.oci.tar: $(call skipable,validate-dockerfile oci-context)
$(outputs_dir)/stage-runtime.oci.tar: docker_image_build_extra_args=$(foreach oci_tag,$(oci_refs),--tag $(oci_tag))

.PHONY: build-oci
build-oci: $(outputs_dir)/stage-build.oci.tar $(outputs_dir)/stage-runtime.oci.tar

.PHONY: validate-oci
validate-oci: $(call skipable,build-oci)
	$(docker_compose) run --rm -T dockle \
		-af settings.py \
		--input /srv/images/stage-runtime.oci.tar

.PHONY: tag-oci
tag-oci:
	$(foreach oci_ref_remote,$(oci_refs_remote),\
		$(docker) tag $(firstword $(oci_refs_local)) $(oci_ref_remote)$(__newline))

.PHONY: push-oci
push-oci:
	$(foreach oci_ref_remote,$(oci_refs_remote),\
		$(docker) push $(oci_ref_remote)$(__newline))

.PHONY: load-oci
load-oci: | $(outputs_dir)/
	find $(outputs_dir) -name '*.oci.tar' -print0 \
		| xargs --no-run-if-empty -0 -n1 -t $(docker) image load -i

.PHONY: run-oci
run-oci: $(call skipable,build-oci)
	$(docker) container run --rm -it \
		$(firstword $(oci_refs_local))

.PHONY: run-oci-sh
run-oci-sh: $(call skipable,build-oci)
	$(docker) container run --rm -it --entrypoint '' \
		$(firstword $(oci_refs_local)) sh
########################################################################
# toolchain
########################################################################

prototool_bin=$(tools_dir)prototool
prototool_version=1.10.0
prototool_qualified=prototool-$(prototool_version)-Linux-x86_64
$(prototool_bin): | $(tools_dir)/ $(XDG_CACHE_HOME)/
	cd $(XDG_CACHE_HOME) \
		&& wget -c -O $(prototool_qualified) \
			https://github.com/uber/prototool/releases/download/v$(prototool_version)/prototool-Linux-x86_64 \
		&& sha256sum -c $(abspath $(prototool_qualified).sha256sum) \
		&& chmod +x $(prototool_qualified)
	cp $(XDG_CACHE_HOME)/$(prototool_qualified) $(@)


.PHONY: toolchain
toolchain: ## install toolchain
toolchain: $(prototool_bin)

.PHONY: distclean
distclean: ## restore repo to pristine state
distclean: clean-$(tools_dir)/

########################################################################
# utility
########################################################################

.PHONY: help
help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'


## clean directories
.PHONY: clean-%/
clean-%/:
	@{ test -d $(*) && { set -x; rm -vr $(*); set +x; } } || echo "directory $(*) does not exist ... nothing to clean"

## create directories
.PRECIOUS: %/
%/:
	mkdir -vp $(@)
