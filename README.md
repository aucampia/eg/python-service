# python-service

```bash
OAUTHLIB_INSECURE_TRANSPORT=1 make run
```

```bash
grpc_address="127.0.0.1:$(poetry run python -m aucampia.eg.service.cli config dump | jq .listen_port)"

# get info
docker-compose run --rm grpcurl -plaintext "${grpc_address}" list
docker-compose run --rm grpcurl -plaintext "${grpc_address}" describe
docker-compose run --rm grpcurl -plaintext "${grpc_address}" describe eg.greeter.v1beta1.GreetRequest
docker-compose run --rm grpcurl -plaintext "${grpc_address}" describe grpc.health.v1.Health.Check
docker-compose run --rm grpcurl -plaintext "${grpc_address}" describe grpc.health.v1.HealthCheckRequest

# call service
docker-compose run --rm grpcurl -plaintext -d '{"name": "caller"}' "${grpc_address}" eg.greeter.v1beta1.GreeterAPI.Greet
docker-compose run --rm grpcurl -plaintext -d '{"service": "eg.greeter.v1beta1.GreeterAPI"}' "${grpc_address}" grpc.health.v1.Health.Check
```


```bash
docker-compose up  mockoauth2

OIDC_ISSUER=http://localhost:31309/issuer

: ${CLIENT_ID:=FAKE_CLIENT_ID}
: ${CLIENT_SECRET:=FAKE_CLIENT_SECRET}
TOKEN_ENDPOINT="$(curl --no-progress-meter "${OIDC_ISSUER}/.well-known/openid-configuration" | tee /dev/stderr | jq -r .token_endpoint)"


curl --no-progress-meter -X POST --url "${TOKEN_ENDPOINT}" \
  --header 'content-type: application/x-www-form-urlencoded' \
  --data grant_type=client_credentials \
  --data "client_id=${CLIENT_ID}" \
  --data "client_secret=${CLIENT_SECRET}" \
  --data audience=https://service.example.com \
  --data scope="extra" \
  | tee /dev/stderr | jq -r .access_token \
  | step crypto jwt inspect --insecure

ACCESS_TOKEN="$(curl --no-progress-meter -X POST --url "${TOKEN_ENDPOINT}" \
  --header 'content-type: application/x-www-form-urlencoded' \
  --data grant_type=client_credentials \
  --data "client_id=${CLIENT_ID}" \
  --data "client_secret=${CLIENT_SECRET}" \
  --data audience=https://service.example.com \
  --data scope="extra" \
  | tee /dev/stderr | jq -r .access_token)"
echo  "${ACCESS_TOKEN}" | step crypto jwt inspect --insecure

make test



```
