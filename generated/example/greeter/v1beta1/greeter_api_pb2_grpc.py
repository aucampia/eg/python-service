# Generated by the gRPC Python protocol compiler plugin. DO NOT EDIT!
"""Client and server classes corresponding to protobuf-defined services."""
import grpc

from example.greeter.v1beta1 import greeter_api_pb2 as example_dot_greeter_dot_v1beta1_dot_greeter__api__pb2


class GreeterAPIStub(object):
    """The greeting service definition.
    """

    def __init__(self, channel):
        """Constructor.

        Args:
            channel: A grpc.Channel.
        """
        self.Greet = channel.unary_unary(
                '/example.greeter.v1beta1.GreeterAPI/Greet',
                request_serializer=example_dot_greeter_dot_v1beta1_dot_greeter__api__pb2.GreetRequest.SerializeToString,
                response_deserializer=example_dot_greeter_dot_v1beta1_dot_greeter__api__pb2.GreetResponse.FromString,
                )


class GreeterAPIServicer(object):
    """The greeting service definition.
    """

    def Greet(self, request, context):
        """Request a greeting.
        """
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')


def add_GreeterAPIServicer_to_server(servicer, server):
    rpc_method_handlers = {
            'Greet': grpc.unary_unary_rpc_method_handler(
                    servicer.Greet,
                    request_deserializer=example_dot_greeter_dot_v1beta1_dot_greeter__api__pb2.GreetRequest.FromString,
                    response_serializer=example_dot_greeter_dot_v1beta1_dot_greeter__api__pb2.GreetResponse.SerializeToString,
            ),
    }
    generic_handler = grpc.method_handlers_generic_handler(
            'example.greeter.v1beta1.GreeterAPI', rpc_method_handlers)
    server.add_generic_rpc_handlers((generic_handler,))


 # This class is part of an EXPERIMENTAL API.
class GreeterAPI(object):
    """The greeting service definition.
    """

    @staticmethod
    def Greet(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_unary(request, target, '/example.greeter.v1beta1.GreeterAPI/Greet',
            example_dot_greeter_dot_v1beta1_dot_greeter__api__pb2.GreetRequest.SerializeToString,
            example_dot_greeter_dot_v1beta1_dot_greeter__api__pb2.GreetResponse.FromString,
            options, channel_credentials,
            insecure, call_credentials, compression, wait_for_ready, timeout, metadata)
