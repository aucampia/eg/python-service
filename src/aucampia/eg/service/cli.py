#!/usr/bin/env python3
# vim: set filetype=python sts=4 ts=4 sw=4 expandtab tw=88 cc=+1:
# vim: set filetype=python tw=88 cc=+1:

import logging
import sys

import typer

from ._version import __version__
from .containers import MainContainer
from .logging import setup_logging

logger = logging.getLogger(__name__)


"""
https://click.palletsprojects.com/en/7.x/api/#parameters
https://click.palletsprojects.com/en/7.x/options/
https://click.palletsprojects.com/en/7.x/arguments/
https://typer.tiangolo.com/
https://typer.tiangolo.com/tutorial/options/
"""


cli = typer.Typer()


@cli.callback()
def cli_callback(
    ctx: typer.Context, verbosity: int = typer.Option(0, "--verbose", "-v", count=True)
) -> None:
    root_logger = logging.getLogger("")
    if verbosity > 0:
        root_logger.propagate = True
        new_level = (
            root_logger.getEffectiveLevel()
            - (min(1, verbosity)) * 10
            - min(max(0, verbosity - 1), 9) * 1
        )
        root_logger.setLevel(new_level)

    logger.debug(
        "info",
        extra={
            "ctx_parent_params": ({} if ctx.parent is None else ctx.parent.params),
            "ctx_params": ctx.params,
            "root_logger_effective_level": logging.getLogger("").getEffectiveLevel(),
            "logger_effective_level": logger.getEffectiveLevel(),
            "version": __version__,
        },
    )


@cli.command("version")
def cli_version(ctx: typer.Context) -> None:
    sys.stderr.write(f"{__version__}\n")


@cli.command("run")
def cli_run(ctx: typer.Context) -> None:

    container = MainContainer.make()
    container.grpc_server().run()


cli_config = typer.Typer()
cli.add_typer(cli_config, name="config")


@cli_config.command("dump")
def cli_config_dump(ctx: typer.Context) -> None:

    container = MainContainer.make()
    sys.stdout.write(f"{container.settings().json()}\n")


# class FileNameRenderer(object):
#     def __init__(self, stack_depth: int):
#         self._stack_depth = stack_depth

#     def __call__(
#         self, logger: structlog.BoundLogger, name: str, event_dict: Dict[str, Any]
#     ) -> Dict[str, Any]:
#         caller = getframeinfo(stack()[self._stack_depth][0])

#         event_dict["file_name"] = f"{caller.filename}:{caller.lineno}"
#         return event_dict


# def add_module_and_lineno(
#     logger: structlog.BoundLogger, name: str, event_dict: Dict[str, Any]
# ) -> Dict[str, Any]:
#     # noinspection PyProtectedMember
#     frame, module_str = structlog._frames._find_first_app_frame_and_name(
#         additional_ignores=[__name__]
#     )
#     # frame has filename, caller and line number
#     event_dict["module"] = module_str
#     event_dict["lineno"] = frame.f_lineno
#     return event_dict


def main() -> None:
    setup_logging()
    # log_handler = logging.StreamHandler()
    # formatter = jsonlogger.JsonFormatter(
    #     datefmt="%Y-%m-%dT%H:%M:%S",
    #     fmt=(
    #         "%(asctime)s %(msecs)03d %(process)d %(thread)x %(levelname)-8s "
    #         "%(name)-12s %(module)s %(lineno)s %(funcName)s %(message)s"
    #     ),
    # )
    # log_handler.setFormatter(formatter)
    # root_logger = logging.getLogger("")
    # root_logger.propagate = True
    # root_logger.setLevel(os.environ.get("PYLOGGING_LEVEL", logging.INFO))
    # root_logger.addHandler(log_handler)

    # # logging.basicConfig(
    # #     format="%(message)s",
    # #     stream=sys.stderr,
    # #     level=os.environ.get("PYLOGGING_LEVEL", logging.INFO),
    # # )
    # structlog.configure(
    #     processors=[
    #         structlog.stdlib.filter_by_level,
    #         # structlog.stdlib.add_logger_name,
    #         # structlog.stdlib.add_log_level,
    #         # structlog.stdlib.PositionalArgumentsFormatter(),
    #         # structlog.processors.TimeStamper(fmt="iso"),
    #         structlog.processors.StackInfoRenderer(),
    #         structlog.processors.format_exc_info,
    #         # # FileNameRenderer(stack_depth=5),  # type: ignore
    #         # # add_module_and_lineno,  # type: ignore
    #         structlog.processors.UnicodeDecoder(),
    #         structlog.stdlib.render_to_log_kwargs,
    #         # structlog.processors.JSONRenderer(),
    #     ],
    #     wrapper_class=structlog.stdlib.BoundLogger,
    #     logger_factory=structlog.stdlib.LoggerFactory(),
    #     cache_logger_on_first_use=True,
    # )

    # slog: FilteringBoundLogger = structlog.getLogger()
    # slog.info("check", pre=True)
    # try:
    #     raise RuntimeError("fake error")
    # except Exception:
    #     slog.error("caught0")
    #     logger.info("caught1", exc_info=True)

    cli()


if __name__ == "__main__":
    main()
