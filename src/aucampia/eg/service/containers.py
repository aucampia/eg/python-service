import logging
import sys

from dependency_injector import containers, providers

from aucampia.eg.service.grpc_service import Greeter, GRPCServer

from .settings import Settings

logger = logging.getLogger(__name__)


class MainContainer(containers.DeclarativeContainer):
    config = providers.Configuration()

    settings = providers.Singleton(Settings)

    greeter = providers.Singleton(Greeter, settings)
    grpc_server = providers.Singleton(GRPCServer, settings, greeter)

    @classmethod
    def make(cls) -> "MainContainer":
        logging.debug("entry ...")
        container = cls()
        container.init_resources()
        container.config.from_pydantic(container.settings())
        container.wire(modules=[sys.modules[__name__]])
        return container
