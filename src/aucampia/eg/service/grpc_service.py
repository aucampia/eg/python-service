import logging
import signal
import threading
from concurrent import futures
from concurrent.futures import ThreadPoolExecutor
from dataclasses import dataclass
from types import FrameType
from typing import Any, Optional

import grpc
import jwt
import structlog
from example.greeter.v1beta1 import greeter_api_pb2

# from authlib.integrations.requests_client import OAuth2Session  # type: ignore[import]
from example.greeter.v1beta1.greeter_api_pb2 import GreetRequest, GreetResponse
from example.greeter.v1beta1.greeter_api_pb2_grpc import (
    GreeterAPIServicer,
    add_GreeterAPIServicer_to_server,
)
from grpc_health.v1 import health  # type: ignore[import]
from grpc_health.v1.health_pb2 import HealthCheckResponse  # type: ignore[import]
from grpc_health.v1.health_pb2_grpc import (  # type: ignore[import]
    add_HealthServicer_to_server,
)
from grpc_reflection.v1alpha import reflection  # type: ignore[import]
from oauthlib.oauth2 import BackendApplicationClient  # type: ignore[import]
from requests_oauthlib import OAuth2Session  # type: ignore[import]
from structlog.types import FilteringBoundLogger

from .settings import Settings

logger: FilteringBoundLogger = structlog.get_logger(__name__)


@dataclass
class Greeter(GreeterAPIServicer):
    settings: Settings

    def __init__(self, settings: Settings) -> None:
        super().__init__()
        self.settings = settings

    def Greet(self, request: GreetRequest, context: Any) -> GreetResponse:
        logger.info("greeting", extra={"request_name": request.name})
        return GreetResponse(
            message=f"Hello {request.name}, my name is {self.settings.name}."
        )


_THREAD_POOL_SIZE = 256


@dataclass
class GRPCServer:
    settings: Settings
    greeter: Greeter

    def auth(self) -> None:

        if self.settings.oauth is None:
            logging.warning("Auth disabled")
            return None

        client_id = self.settings.oauth.client_id
        client_secret = self.settings.oauth.client_secret
        token_endpoint = self.settings.oauth.token_endpoint
        client = BackendApplicationClient(
            client_id=client_id, scope="a:action b:action"
        )
        # OAUTHLIB_INSECURE_TRANSPORT
        oauth = OAuth2Session(client=client)
        token = oauth.fetch_token(
            token_url=token_endpoint,
            client_id=client_id,
            client_secret=client_secret,
        )
        access_token = token["access_token"]

        jwks_client = jwt.PyJWKClient(self.settings.oauth.jwks_uri)
        signing_key = jwks_client.get_signing_key_from_jwt(access_token)
        logger.debug(
            "data",
            signing_key={
                attr: getattr(signing_key, attr)
                for attr in ("Algorithm", "key_type", "key_id")
            },
        )
        token_data = jwt.decode(
            access_token,
            signing_key.key,
            algorithms=["RS256"],
            options={"verify_aud": False},
        )
        logger.debug("data", token_data=token_data)

    def run(self) -> None:
        server = grpc.server(ThreadPoolExecutor(max_workers=10))

        health_servicer = health.HealthServicer(
            experimental_non_blocking=True,
            experimental_thread_pool=futures.ThreadPoolExecutor(
                max_workers=_THREAD_POOL_SIZE
            ),
        )

        self.auth()

        add_GreeterAPIServicer_to_server(self.greeter, server)

        service_names = [
            service.full_name
            for service in greeter_api_pb2.DESCRIPTOR.services_by_name.values()
        ] + [reflection.SERVICE_NAME, health.SERVICE_NAME]

        logger.debug("data", service_names=service_names)
        add_HealthServicer_to_server(health_servicer, server)
        for service_name in service_names:
            health_servicer.set(service_name, HealthCheckResponse.SERVING)
        reflection.enable_server_reflection(service_names, server)

        if self.settings.tls is not None:
            tls = self.settings.tls
            credentials = grpc.ssl_server_credentials(
                ((tls.key_file.read_bytes(), tls.cert_file.read_bytes()),)
            )
            server.add_secure_port(self.settings.listen_address, credentials)
        else:
            server.add_insecure_port(self.settings.listen_address)

        shutdown_event = threading.Event()

        def do_shutdown(signum: int, frame: Optional[FrameType]) -> None:
            logger.info("shutdown requested", extra={"signum": signum})
            shutdown_event.set()

        try:
            old_term = signal.signal(signal.SIGTERM, do_shutdown)
            old_int = signal.signal(signal.SIGINT, do_shutdown)
            logger.info("starting")
            logging.info("starting 2.0")
            server.start()
            shutdown_event.wait()
            logger.info("stopping server")
            server.stop(self.settings.shutdown_timeout).wait()
            logger.info("stopped")
        finally:
            signal.signal(signal.SIGTERM, old_term)
            signal.signal(signal.SIGINT, old_int)
