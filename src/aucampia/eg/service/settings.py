import logging
import os
from pathlib import Path
from typing import Any, Dict, Optional, Tuple

import yaml
from pydantic import BaseSettings, Field
from pydantic.env_settings import EnvSettingsSource, SettingsSourceCallable
from pydantic.main import BaseModel
from pydantic.networks import AnyHttpUrl, AnyUrl
from pydantic.types import SecretStr
from typing_extensions import Annotated

logger = logging.getLogger(__name__)


class TLSSettings(BaseModel):
    cert_file: Path
    key_file: Path


class OAuthSettings(BaseModel):
    token_endpoint: AnyHttpUrl
    client_id: str
    client_secret: SecretStr
    jwks_uri: AnyHttpUrl
    audience: AnyUrl


class Settings(BaseSettings):
    listen_port: Annotated[
        int, Field(gt=0, lt=(2**16), extra={"env_names": "PORT"})
    ] = 31174
    listen_host: str = "127.0.0.1"
    tls: Optional[TLSSettings] = None
    oauth: Optional[OAuthSettings] = None
    name: str = "egsvc"
    shutdown_timeout: int = 5

    @property
    def listen_address(self) -> str:
        return f"{self.listen_host}:{self.listen_port}"

    class Config:
        env_prefix = "egsvc_"
        env_file = ".env"

        @classmethod
        def yaml_config_settings_source(cls, settings: BaseSettings) -> Dict[str, Any]:
            if "egsvc_config" not in os.environ:
                return {}
            settings_file = Path(os.environ["egsvc_config"])
            logger.debug("settings_file = %s", settings_file)
            if not settings_file.exists():
                return {}
            with settings_file.open() as fileh:
                settings_data = yaml.safe_load(fileh)
            if not isinstance(settings_data, dict):
                raise ValueError(f"settings file {settings_file} must contain a dict")
            return settings_data

        @classmethod
        def customise_sources(
            cls,
            init_settings: SettingsSourceCallable,
            env_settings: SettingsSourceCallable,
            file_secret_settings: SettingsSourceCallable,
        ) -> Tuple[SettingsSourceCallable, ...]:
            return (
                init_settings,
                env_settings,
                EnvSettingsSource(env_file="default.env", env_file_encoding=None),
                cls.yaml_config_settings_source,
                file_secret_settings,
            )
