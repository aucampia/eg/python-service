import json
import logging
import os
from contextlib import contextmanager
from pathlib import Path, PurePath
from typing import Dict, Iterator, Mapping, Optional, TypeVar, Union

import pytest
from pytest_subtests import SubTests  # type: ignore[import]

from aucampia.eg.service.settings import Settings


class EnvUtils:
    @classmethod
    def set(cls, key: str, value: Optional[str]) -> Optional[str]:
        original = os.environ.get(key, None)
        if value is None:
            del os.environ[key]
        else:
            os.environ[key] = value
        return original

    @classmethod
    def setall(cls, envmod: Mapping[str, Optional[str]]) -> Mapping[str, Optional[str]]:
        backups: Dict[str, Optional[str]] = {}
        for key, value in envmod.items():
            backup = backups[key] = os.environ.get(key, None)
            if value is None and backup is not None:
                del os.environ[key]
            elif value is not None:
                os.environ[key] = value
        return backups

    @classmethod
    @contextmanager
    def ctx_setall(cls, envmod: Mapping[str, Optional[str]]) -> Iterator[None]:
        backup = cls.setall(envmod)
        try:
            yield None
        finally:
            cls.setall(backup)


PathLike = Union[PurePath, str]
PathLikeT = TypeVar("PathLikeT", bound=PathLike)


@contextmanager
def ctx_chdir(newdir: PathLikeT) -> Iterator[PathLikeT]:
    cwd = os.getcwd()
    try:
        os.chdir(f"{newdir}")
        yield newdir
    finally:
        os.chdir(cwd)


def test_config(tmp_path: Path, subtests: SubTests) -> None:
    with pytest.raises(ValueError):
        settings = Settings.parse_obj({"listen_port": -123, "listen_host": "000"})
        logging.info("settings = %s", settings)

    cleanenv = {
        "egsvc_name": None,
        "egsvc_listen_port": None,
        "egsvc_listen_host": None,
        "egsvc_tls": None,
        "egsvc_tls_enabled": None,
        "egsvc_tls_cert_file": None,
        "egsvc_tls_key_file": None,
        "egsvc_oauth": None,
        "egsvc_oauth_token_endpoint": None,
        "egsvc_oauth_client_id": None,
        "egsvc_oauth_client_secret": None,
        "egsvc_oauth_jwks_uri": None,
    }

    with EnvUtils.ctx_setall(cleanenv), ctx_chdir(tmp_path):
        settings = Settings()
        logging.info("settings = %s", json.dumps(settings.dict(), indent=2))
        assert settings.oauth is None
        assert settings.tls is None

    with EnvUtils.ctx_setall(
        {
            **cleanenv,
            "egsvc_oauth": json.dumps(
                {
                    "token_endpoint": "http://127.0.0.1:31309/.well-known/openid-configuration/token",
                    "client_id": "egsvcid",
                    "client_secret": "1234",
                    "audience": "https://service.example.com/",
                    "jwks_uri": "http://127.0.0.1:31309/.well-known/openid-configuration/jwks",
                }
            ),
            "egsvc_tls": json.dumps(
                {
                    "cert_file": "/var/tmp/cert.pem",
                    "key_file": "/var/tmp/key.pem",
                }
            ),
        }
    ), ctx_chdir(tmp_path):
        settings = Settings()
        logging.info("settings = %s", settings.json(indent=2))
        egsvc_oauth_obj = json.loads(os.environ["egsvc_oauth"])
        egsvc_tls_obj = json.loads(os.environ["egsvc_tls"])
        assert settings.oauth is not None
        assert {
            **json.loads(settings.oauth.json()),
            "client_secret": settings.oauth.client_secret.get_secret_value(),
        } == egsvc_oauth_obj
        assert settings.tls is not None
        assert json.loads(settings.tls.json()) == egsvc_tls_obj
